<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('admin/dashboard');
});

Route::get('/home', function () {
    return redirect('admin/dashboard');
});
Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

//Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::group([
    'prefix' => config('backpack.base.route_prefix', 'admin'),
    'middleware' => ['admin'],
    'namespace' => 'Admin'
], function () {
    CRUD::resource('menu', 'MenuCrudController');
    Route::get('dashboard', function () {

		$user = Auth::user()->id;  //user()->email;//user()->id;
        $_menus = App\Models\Menu::select('id','menu', 'icon')
                                ->where('user_id', $user)//where('user_email',$user)//
                                ->get();
        $menus = array();
            foreach ($_menus as  $value) {
               $menus[] = ['id' => $value->id, 'menu' => $value->menu, 'icon' => $value->icon];
            }
                $menus['menu_name'] = 'dashboard';
//print_r($menus);die();

        return view('vendor/backpack/base/dashboard')
            ->with('data', $menus);

    });

   
    Route::get('user/menu/{id}', 'MenuCrudController@userMenu');
    Route::get('menu/delete/{id}', 'MenuCrudController@deleteMenu');
    Route::get('menu/show/{id}', 'MenuCrudController@showMenu');

    Route::get('test', 'MenuCrudController@test');


});