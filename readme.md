<h1> Backpack-Demo </h1>
<p>Backpack-Demo is just a demonstration of Laravel-Backpack. You are provided with an admin account. Here you can add your clients and create customized menu for each of them. 
Remember, this is  just a demonstration. So feel free to play with it!</p>
![](public/readme/Backpack-Demo.JPG)
