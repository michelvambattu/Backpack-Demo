@extends('backpack::layout')

@section('header')
  <section class="content-header">
    <h1>

      <span class="text-capitalize">{{ $data['name'] }}</span>
      <small>Menu list</small>
    </h1>
    
  </section>
@endsection

@section('content')
<!-- Default box -->
  <div class="row">

    <!-- THE ACTUAL CONTENT -->
    <div class="col-md-12">
      <div class="box">
        <div class="box-header ">
          <a href="{{ url('/admin/menu/create?email='. $data['email']) }}" class="btn btn-primary ladda-button" data-style="zoom-in"><span class="ladda-label"><i class="fa fa-plus"></i> Add Menu</span></a>
        </div>
        <div class="box-header">
        <a href="{{ url('/admin/user') }}"><i class="fa fa-angle-double-left"></i> Back to client list</a>
        </div>

        <div class="box-body table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Menu</th>
                <th>Icon</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              @if ($data['menuList'])
                    @foreach ($data['menuList'] as $key => $entry)
                  <tr>
                    <td>{{ $entry['menu'] }}</td> 
                    <td>{{ $entry['icon'] }}</td> 
                    <td>
                        <a href="{{ url('/admin/menu/'. $entry['id']) .'/edit?email='. $data['email'] }}" class="btn btn-xs btn-default">
                        <i class="fa fa-list"></i> Edit</a>
                    </td> 
                     <td>
                        <a href="{{ url('/admin/menu/delete/' . $entry['id']) }}" class="btn btn-xs btn-default" data-button-type="delete"><i class="fa fa-trash"></i> Delete</a>
                    </td>

                  </tr>
                @endforeach
              @else
                <tr>
                  <td colspan="3" align="center">No records found!</td>
                </tr>
              @endif
            </tbody>
          </table>

        </div>

      </div><!-- /.box -->
    </div>

  </div>

@endsection



