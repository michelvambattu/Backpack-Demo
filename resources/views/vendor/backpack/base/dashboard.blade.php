@extends('backpack::layout')

@role('Admin')
@section('header')
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>
      </ol>
    </section>
@endsection
@endrole

@section('content')
@role('Client')
@if($data)
@if($data['menu_name'] == 'dashboard')

<div style="text-align:center;">
	<img src="{{ asset('menu/DASHBOARD.JPG')}}"  alt="dashboard" >
</div>
@elseif($data['menu_name'] == 'Accounting')
<div style="text-align:center;">
	<img src="{{ asset('menu/ACCOUNTING.PNG')}}"  alt="dashboard" style="width:65%;" >
</div>
@elseif($data['menu_name'] == 'Sales')
<div style="text-align:center;">
	<img src="{{ asset('menu/Sales.jpg')}}"  alt="dashboard" style="width:65%;">
</div>
@elseif($data['menu_name'] == 'Opportunity')
<div style="text-align:center;">
	<img src="{{ asset('menu/Opportunity.jpg')}}"  alt="dashboard" >
</div>
@elseif($data['menu_name'] == 'Financial')
<div style="text-align:center;">
	<img src="{{ asset('menu/Financial.png')}}"  alt="dashboard" >
</div>
@elseif($data['menu_name'] == 'Payroll')
<div style="text-align:center;">
  <img src="{{ asset('menu/Payroll.png')}}"  alt="dashboard" >
</div>
@elseif($data['menu_name'] == 'Warehouse')
<div style="text-align:center;">
  <img src="{{ asset('menu/Warehouse.png')}}"  alt="dashboard" >
</div>
@elseif($data['menu_name'] == 'Inventory')
<div style="text-align:center;">
  <img src="{{ asset('menu/Inventory.png')}}"  alt="dashboard" >
</div>

@endif
@endif
@endrole
@endsection


