@if (Auth::check())
    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        @include('backpack::inc.sidebar_user_panel')

        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
          {{-- <li class="header">{{ trans('backpack::base.administration') }}</li> --}}
          <!-- ================================================ -->
          <!-- ==== Recommended place for admin menu items ==== -->
          <!-- ================================================ -->
          <li><a href="{{ backpack_url('dashboard') }}"><i class="fa fa-dashboard"></i> <span>{{ trans('backpack::base.dashboard') }}</span></a></li>

 		@role('Client')
      @if($data)
          @foreach($data as $k => $value)
          @if(is_int($k))
           <li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/menu/show/' .$value['id']) }}"><i class="fa {{$value['icon']}}"></i> <span>{{ $value['menu'] }}</span></a></li>
           @endif
          @endforeach
         @endif  
		@endrole



         
 
       	@role('Admin')
           	<li><a href="{{ url(config('backpack.base.route_prefix', 'admin').'/user') }}"><i class="fa fa-list"></i> <span>Menu</span></a></li>

          <!-- ======================================= -->
        
         <!-- Users, Roles Permissions -->
          
           	<li><a href="{{ url(config('backpack.base.route_prefix', 'admin') . '/user') }}"><i class="fa fa-user"></i> <span>Clients</span></a></li>
             
		@endrole

        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>
@endif

