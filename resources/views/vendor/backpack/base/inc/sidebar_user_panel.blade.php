<div class="user-panel">
	

	@if( Auth::user()->logo)
  	<a class="pull-left image"  href="">

	<img src="{{ asset('uploads/'. Auth::user()->logo) }}" class="img-round" alt="User Image" style="width: 36px;">
  	</a>
  	<div class="pull-left info">
    <p><a href="">{{ Auth::user()->name }}</a></p>
    @else
    <a class="pull-left image"  href="">

	<img src="{{ backpack_avatar_url(Auth::user()) }}" class="img-circle" alt="User Image">
  	</a>
  	<div class="pull-left info">
    <p><a href="">{{ Auth::user()->name }}</a></p>
@endif

  @hasrole('Admin')
     <a href="{{ backpack_url('logout') }}"><i class="fa fa-sign-out"></i> <span>{{ trans('backpack::base.logout') }}</span></a>
    @endhasrole
  </div>
</div>