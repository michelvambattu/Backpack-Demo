@extends('backpack::layout')

@section('header')
    <section class="content-header">
 

@role('Admin')
      <ol class="breadcrumb">
        <li><a href="{{ backpack_url() }}">{{ config('backpack.base.project_name') }}</a></li>
        <li class="active">{{ trans('backpack::base.dashboard') }}</li>

@endrole
      </ol>
    </section>
@endsection

