<?php

namespace App\Http\Controllers\Admin;

use Backpack\CRUD\app\Http\Controllers\CrudController;

// VALIDATION: change the requests to match your own file names if you need form validation
use App\Http\Requests\MenuRequest as StoreRequest;
use App\Http\Requests\MenuRequest as UpdateRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Menu;

class MenuCrudController extends CrudController
{

   

   


    public function setup()
    {

             $request = request();


        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */
        $this->crud->setModel('App\Models\Menu');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/menu');
        $this->crud->setEntityNameStrings('menu', 'menus');

        /*
        |--------------------------------------------------------------------------
        | BASIC CRUD INFORMATION
        |--------------------------------------------------------------------------
        */

        $this->crud->setFromDb();

        // ------ CRUD FIELDS

        $user_id = DB::table('users')->where('email', $request->email)->value('id'); 

         $this->crud->addField([
            'name'      => 'user_id',  
            'label'     =>  'Choose Company',
            'type'      =>  'select2_from_array',
            'options'   => [$user_id    =>  $request->email],
                                    ], 'both');
          $this->crud->addField([
            'name'      => 'icon',
            'label'     =>  'Icon',
            'type'      =>  'icon_picker',
            'value'     =>  'fa-list'
           
                                    ], 'create');
           $this->crud->addField([
            'name'      => 'icon',
            'label'     =>  'Icon',
            'type'      =>  'icon_picker',
            'value'     =>  'fa-list'
           
                                    ], 'update');



        
    }

    public function store(StoreRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::storeCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry

        //print_r($redirect_location);die();
        return redirect('admin/user');
    }

    public function update(UpdateRequest $request)
    {
        // your additional operations before save here
        $redirect_location = parent::updateCrud($request);
        // your additional operations after save here
        // use $this->data['entry'] or $this->crud->entry
        return redirect('admin/user');
    }


    public function userMenu($id){

        $menuList = array();
        $user = DB::table('users')->where('id',$id)->first();
        $_user_menu = DB::table('user_menu')
                        ->where('user_id', $user->id)->get(); 
        
        foreach ($_user_menu as $value) {
            $menuList[] = ['id' => $value->id,'menu' => $value->menu, 'icon' => $value->icon];
        }

        $user_menu['name'] =  $user->name;
        $user_menu['email'] = $user->email;
        $user_menu['menuList'] = $menuList;

 
        return view('vendor/backpack/base/menu_list')
                ->with('data', $user_menu);

    }

    public function deleteMenu($id){

        $user_id = DB::table('user_menu')
                    ->join('users', 'users.id','=','user_menu.user_id') 
                    ->where('user_menu.id' ,$id)
                    ->value('users.id');

        DB::table('user_menu')->where('id' ,$id)->delete();

        return redirect()->action(
            'Admin\MenuCrudController@userMenu', ['id' => $user_id]
        );
    }


    public function showMenu($id){

        $menus = array();
        $user = Auth::user()->id; 
        $_menus = Menu::select('id','menu', 'icon')
                                ->where('user_id', $user)
                                ->get();
        
        foreach ($_menus as  $value) {
           $menus[] = ['id' => $value->id, 'menu' => $value->menu, 'icon' => $value->icon];
        }

        $first_menu = DB::table('user_menu')->where('id',$id)->first();
        $menus['menu_name'] = $first_menu->menu;
        $menus['menu_id'] = $first_menu->id;

        return view('vendor/backpack/base/dashboard')
                ->with('data', $menus);
            
    }
    
}
